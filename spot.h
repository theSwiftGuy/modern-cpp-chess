#ifndef SPOT_H
#define SPOT_H

#include <QLabel>
#include <QDebug>
#include <QMessageBox>
#include <QWidget>
#include <Qt>
#include <optional>
#include "piece.h"

class Spot : public QLabel
{
    Q_OBJECT

public:
    //Constructors
    Spot(int row = 0, int column = 0,  QWidget* pParent = nullptr, Qt::WindowFlags f = nullptr);
    Spot(const QString& text, int row = 0, int column = 0, QWidget* pParent = nullptr, Qt::WindowFlags f = nullptr);

    Spot( const Spot &obj);
    Spot& operator=(const Spot&other);

signals:
    void clicked(char pieceName);
    void spotSelected(unsigned int indexPath);
    void spotReleased(unsigned int indexPath);

//Methods
private:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

public:
    void setPiece(std::shared_ptr<Piece> piece);
    std::optional<std::shared_ptr<Piece>> getPiece();
    void removePiece();
    void spotDisplay();
    void isSelected(bool value);
    void colourMovesPath();

//Fields
public:
    int num;
    int row;
    int col;

private:
    std::optional<std::shared_ptr<Piece>> _piece = std::nullopt;
    int color;
};

#endif // SPOT_H
