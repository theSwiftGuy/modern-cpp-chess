#include "pawn.h"
#include "gamecontroller.h"

Moves Pawn::allMoves(int row, int col) {
    Moves moves;

    auto spots = GameController::get()._spots;
    auto index = row * 8 + col;

    int r,c;
    r=row;
    c=col;

    if ( _colour == white) {
        //white pawn context
        if((index + 8 <= 63) && !spots[index + 8]->getPiece())
        {
            moves.pawnMoveModeIndexes.emplace_back(index+8);
            if((index >= 8 && index <=15) && !spots[index + 8 + 8]->getPiece())
                moves.pawnMoveModeIndexes.emplace_back(index+8+8);
        }

        if((index + 9 <= 63))
        {
            if(spots[index + 8 + 1]->getPiece()) {
                if(spots[index + 8 + 1]->getPiece()->get()->_colour != _colour) {
                    moves.pawnMoveModeIndexes.emplace_back(index+8+1);
                } else {
                    moves.protectedIndexes.emplace_back(index+8+1);
                }
            } else {
                moves.pawnAttackModeIndexes.emplace_back(index+8+1);
            }

        }
        if((index + 7 <= 63))
        {
            if(spots[index + 8 - 1]->getPiece()) {
                if(spots[index + 8 - 1]->getPiece()->get()->_colour != _colour) {
                    moves.pawnMoveModeIndexes.emplace_back(index+8-1);
                } else {
                    moves.protectedIndexes.emplace_back(index+8-1);
                }
            } else {
                moves.pawnAttackModeIndexes.emplace_back(index+8-1);
            }
        }

        if(row==4 && (spots[index-1]->getPiece()
                      && spots[index - 1]->getPiece()->get()->_colour != _colour
                      && spots[index - 1]->getPiece()->get()->_type == pawn
                      && spots[index - 1]->getPiece()->get()->enPassant == true))
        {
           moves.pawnMoveModeIndexes.emplace_back(index +8 -1);
        }
        if(row==4 && (spots[index+1]->getPiece()
                      && spots[index + 1]->getPiece()->get()->_colour != _colour
                      && spots[index + 1]->getPiece()->get()->_type == pawn
                      && spots[index + 1]->getPiece()->get()->enPassant == true))
        {
           moves.pawnMoveModeIndexes.emplace_back(index +8 +1);
        }

    }
    else {
        //black pawn context
        if((index -8 >= 0) && !spots[index - 8]->getPiece())
        {
            moves.pawnMoveModeIndexes.emplace_back(index-8);
            if((index >= 48 && index <=55) && !spots[index - 8 - 8]->getPiece())
                moves.pawnMoveModeIndexes.emplace_back(index-8-8);
        }

        if((index - 7 >= 0))
        {
            if(spots[index - 8 + 1]->getPiece()) {
                if(spots[index - 8 + 1]->getPiece()->get()->_colour != _colour) {
                    moves.pawnMoveModeIndexes.emplace_back(index-8+1);
                } else {
                    moves.protectedIndexes.emplace_back(index-8+1);
                }
            } else {
                moves.pawnAttackModeIndexes.emplace_back(index-8+1);
            }
        }
        if((index - 9 >= 0))
        {
            if(spots[index - 8 - 1]->getPiece()) {
            if(spots[index - 8 - 1]->getPiece()->get()->_colour != _colour) {
                moves.pawnMoveModeIndexes.emplace_back(index-8-1);
            } else {
                moves.protectedIndexes.emplace_back(index-8-1);
            }
            } else {
                moves.pawnAttackModeIndexes.emplace_back(index-8-1);
            }
        }

        if(row==3 && (spots[index-1]->getPiece()
                      && spots[index - 1]->getPiece()->get()->_colour != _colour
                      && spots[index - 1]->getPiece()->get()->_type == pawn
                      && spots[index - 1]->getPiece()->get()->enPassant == true))
        {
           moves.pawnMoveModeIndexes.emplace_back(index- 8-1);
        }
        if(row==3 && (spots[index+1]->getPiece()
                      && spots[index + 1]->getPiece()->get()->_colour != _colour
                      && spots[index + 1]->getPiece()->get()->_type == pawn
                      && spots[index + 1]->getPiece()->get()->enPassant == true))
        {
           moves.pawnMoveModeIndexes.emplace_back(index- 8+1);
        }

    }

    return moves;
}

std::vector<int> Pawn::chessPathMoves(int row, int col,[[maybe_unused]] int kingRow,[[maybe_unused]] int kingCol) {
    return {row * 8 + col};
}
