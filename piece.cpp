#include "piece.h"
#include "gamecontroller.h"
#include <string>

std::string PieceType(Type type) {
   switch(type) {
      case pawn:
         return "pawn";
      case rook:
         return "rook";
      case knight:
         return "knight";
      case bishop:
         return "bishop";
      case queen:
         return "queen";
      case king:
         return "king";
      default:
        return "invalid type";
   }
}

std::string PieceColour(Colour colour) {
   switch(colour) {
      case white:
         return "white";
      case black:
         return "black";
      default:
        return "na";
   }
}

//Piece::~Piece()
//{
//    printf(" Piece destructor... \n");
//}

std::string Piece::imagePath() {
   const std::string pathPrefix(":/img/");
   const std::string pathExtension(".svg");

   return (pathPrefix + PieceType(_type) + "_" + PieceColour(_colour) + pathExtension);
}

bool Piece::isTypeAndColour(Type type, Colour colour)
{
    return (_type == type && _colour == colour);
}

bool Piece::addMoves(unsigned int index, Colour colour, Moves &moves) { //std::vector<int> &attackedList, std::vector<int> &protectedList) {

    auto spots = GameController::get()._spots;

    if(!spots[index]->getPiece())
    {
        moves.attackedIndexes.emplace_back(index);
        return true;
    }

    else if(spots[index]->getPiece() && spots[index]->getPiece()->get()->_colour == colour)
    {
        moves.protectedIndexes.emplace_back(index);
        return false;
    }

    else if(spots[index]->getPiece() && spots[index]->getPiece()->get()->_colour != colour)
    {
        moves.attackedIndexes.emplace_back(index);
        return false;
    }
}

bool Piece::addChessMoves(unsigned int index, Colour colour, std::vector<int> &list) {

    auto spots = GameController::get()._spots;

    if(!spots[index]->getPiece())
    {
        list.emplace_back(index);
        return true;
    } else {
        return false;
    }
}

void Piece::findMoves(int row, int column, std::function<bool (int&)> funcRow, std::function<bool (int&)> funcCol, Colour _colour,
                      Moves &moves) {

    int r,c;
    r = row;
    c = column;

    while(funcRow(r) && funcCol(c))
    {
        if(!addMoves(static_cast<unsigned int>(r * 8 + c), _colour, moves))
            break;
    }
}

void Piece::findChessMoves(int row, int column, std::function<bool (int&)> funcRow, std::function<bool (int&)> funcCol, Colour _colour,
                      std::vector<int> &list) {

    int r,c;
    r = row;
    c = column;

    while(funcRow(r) && funcCol(c))
    {
        if(!addChessMoves(static_cast<unsigned int>(r * 8 + c), _colour, list))
            break;
    }
}
