#ifndef PIECEFACTORY_H
#define PIECEFACTORY_H

#include "pawn.h"
#include "rook.h"
#include "bishop.h"
#include "knight.h"
#include "queen.h"
#include "king.h"

class PieceFactory
{
public:
    static std::unique_ptr<Piece> create(Type pieceType, Colour pieceColour)
    {
        std::unique_ptr<Piece> piece;
        switch(pieceType) {
        case pawn:
            piece = std::make_unique<Pawn>(pieceColour);
            break;
        case rook:
            piece = std::make_unique<Rook>(pieceColour);
            break;
        case bishop:
            piece = std::make_unique<Bishop>(pieceColour);
            break;
        case knight:
            piece = std::make_unique<Knight>(pieceColour);
            break;
        case queen:
            piece = std::make_unique<Queen>(pieceColour);
            break;
        case king:
            piece = std::make_unique<King>(pieceColour);
            break;
        case none:
        default:
          break;
        }
        return piece;
    }

};

#endif // PIECEFACTORY_H
