#ifndef BISHOP_H
#define BISHOP_H
#include "piece.h"

class Bishop : public Piece
{
public:
    Bishop(Colour colour, Type type = Type::bishop) : Piece(colour, type) {}
    virtual ~Bishop() override {}
    Moves allMoves(int row, int col) override;
    std::vector<int> chessPathMoves(int row, int col, int kingRow, int kingCol) override;
};

#endif // BISHOP_H
