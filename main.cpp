#include "mainwindow.h"
#include "gamecontroller.h"
#include <QApplication>
#include <QPointer>
#include <iostream>
#include <algorithm>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    const auto spotSize = 64;
    const auto windowSize = 700;

    QWidget * appWidget = new QWidget();
    appWidget->setGeometry(0, 0, windowSize, windowSize);

    GameController::get(appWidget, spotSize);

    appWidget->show();

    return app.exec();
}
