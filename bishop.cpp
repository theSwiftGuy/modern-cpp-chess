#include "bishop.h"

Moves Bishop::allMoves(int row, int col) {
    Moves moves;

    auto decreaseRow = [](int &r) { return r-->0; };
    auto increaseRow = [](int &r) { return r++<7; };
    auto increaseColumn = [](int &c) { return c++<7; };
    auto decreaseColumn = [](int &c) { return c-->0; };

    findMoves(row, col, decreaseRow, increaseColumn, _colour, moves);
    findMoves(row, col, increaseRow, increaseColumn, _colour, moves);
    findMoves(row, col, decreaseRow, decreaseColumn, _colour, moves);
    findMoves(row, col, increaseRow, decreaseColumn, _colour, moves);

    return moves;
}

std::vector<int> Bishop::chessPathMoves(int row, int col, int kingRow, int kingCol) {
    std::vector<int> chessPath;
    chessPath.emplace_back(row * 8 + col);

    auto decreaseRow = [&](int &r) { return r-->kingRow; };
    auto increaseRow = [&](int &r) { return r++<kingRow; };
    auto increaseColumn = [&](int &c) { return c++<kingCol; };
    auto decreaseColumn = [&](int &c) { return c-->kingCol; };

    findChessMoves(row, col, decreaseRow, increaseColumn, _colour, chessPath);
    findChessMoves(row, col, increaseRow, increaseColumn, _colour, chessPath);
    findChessMoves(row, col, decreaseRow, decreaseColumn, _colour, chessPath);
    findChessMoves(row, col, increaseRow, decreaseColumn, _colour, chessPath);

    return chessPath;
}
