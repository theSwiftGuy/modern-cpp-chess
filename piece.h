#ifndef PIECE_H
#define PIECE_H

#include <string_view>
#include <string>
#include <vector>
#include <functional>

enum Colour {nonColour, white = 1, black = 2};
enum Type {none, pawn = 1, rook, knight, bishop, queen, king};

struct Moves {
    std::vector<int> attackedIndexes;
    std::vector<int> protectedIndexes;
    std::vector<int> pawnMoveModeIndexes;
    std::vector<int> pawnAttackModeIndexes;

    std::vector<int> attackIndexes() {
        std::vector<int> attackIndexes;
        std::set_union(attackedIndexes.begin(), attackedIndexes.end(),
                       pawnAttackModeIndexes.begin(), pawnAttackModeIndexes.end(),
                       std::back_inserter(attackIndexes));

        return attackIndexes;
    }

    std::vector<int> movementIndexes() {
        std::vector<int> movementIndexes;
        std::set_union(attackedIndexes.begin(), attackedIndexes.end(),
                       pawnMoveModeIndexes.begin(), pawnMoveModeIndexes.end(),
                       std::back_inserter(movementIndexes));
        return movementIndexes;
    }

    std::vector<int> protectIndexes() {
        return protectedIndexes;
    }
};

class Piece
{
public:
    Piece(Colour colour, Type type = none) : _colour(colour), _type(type) {}
    virtual ~Piece() {}
    virtual Moves allMoves(int row, int col) = 0;
    virtual std::vector<int> chessPathMoves(int row, int col, int kingRow, int kingCol) = 0;

    std::string imagePath();
    bool isTypeAndColour(Type type, Colour colour);

protected:
    bool addMoves(unsigned int index, Colour colour, Moves &moves);
    bool addChessMoves(unsigned int index, Colour colour, std::vector<int> &list);
    void findMoves(int row, int column, std::function<bool (int&)> funcRow, std::function<bool (int&)> funcCol,
                   Colour _colour, Moves &moves);
    void findChessMoves(int row, int column, std::function<bool (int&)> funcRow, std::function<bool (int&)> funcCol, Colour _colour,
                          std::vector<int> &list);

public:
    Colour _colour = nonColour;
    Type _type = none;
    bool enPassant = false;

};

#endif // PIECE_H
