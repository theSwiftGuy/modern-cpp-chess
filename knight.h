#ifndef KNIGHT_H
#define KNIGHT_H
#include "piece.h"

class Knight : public Piece
{
public:
    Knight(Colour colour, Type type = Type::knight) : Piece(colour, type) {}
    virtual ~Knight() override {}
    Moves allMoves(int row, int col) override;
    std::vector<int> chessPathMoves(int row, int col, int kingRow, int kingCol) override;
};

#endif // KNIGHT_H
