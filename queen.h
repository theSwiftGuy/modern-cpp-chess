#ifndef QUEEN_H
#define QUEEN_H
#include "piece.h"

class Queen : public Piece
{
public:
    Queen(Colour colour, Type type = Type::queen) : Piece(colour, type) {}
    virtual ~Queen() override {}
    Moves allMoves(int row, int col) override;
    std::vector<int> chessPathMoves(int row, int col, int kingRow, int kingCol) override;
};

#endif // QUEEN_H
