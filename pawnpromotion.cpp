#include <QtGui>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QBoxLayout>
#include <QListWidget>
#include <QListView>
#include "pawnpromotion.h"
#include "gamecontroller.h"

PawnPromotion::PawnPromotion(int color, QWidget *parent) : QDialog(parent), pieceColor(color)
{
    printf("constructor of PawnPromotionDialog \n");

    contentsWidget = new QListWidget;
    contentsWidget->setViewMode(QListView::ListMode);
    contentsWidget->setIconSize(QSize(64, 64));
    contentsWidget->setMovement(QListView::Static);
    contentsWidget->setFlow(QListWidget::LeftToRight);

    QListWidgetItem *queen = new QListWidgetItem(contentsWidget);
    queen->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    queen->setBackgroundColor(QColor(173, 136, 94, 255));

    QListWidgetItem *rook = new QListWidgetItem(contentsWidget);
    rook->setFlags(Qt::ItemIsEnabled | Qt::ItemIsEnabled);
    rook->setBackgroundColor(QColor(238, 216, 176));

    QListWidgetItem *bishop = new QListWidgetItem(contentsWidget);
    bishop->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    bishop->setBackgroundColor(QColor(173, 136, 94, 255));

    QListWidgetItem *knight = new QListWidgetItem(contentsWidget);
    knight->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    knight->setBackgroundColor(QColor(238, 216, 176));

    if(pieceColor == 1)
    {
        queen->setIcon(QIcon(":/img/queen_white.svg"));
        rook->setIcon(QIcon(":/img/rook_white.svg"));
        bishop->setIcon(QIcon(":/img/bishop_white.svg"));
        knight->setIcon(QIcon(":/img/knight_white.svg"));;
    }
    else
    {
        queen->setIcon(QIcon(":/img/queen_black.svg"));
        rook->setIcon(QIcon(":/img/rook_black.svg"));
        bishop->setIcon(QIcon(":/img/bishop_black.svg"));
        knight->setIcon(QIcon(":/img/knight_black.svg"));
    }

    QVBoxLayout *topLeftLayout = new QVBoxLayout;
    topLeftLayout->addWidget(contentsWidget);

    connect(contentsWidget, SIGNAL(itemClicked(QListWidgetItem*)),
        this,SLOT(processItem(QListWidgetItem*)));
    connect(this, &PawnPromotion::promotePawn, &GameController::get(), &GameController::promotePawn);

    setLayout(topLeftLayout);
    setWindowTitle(tr("Pawn Promotion"));
    setModal(true);
    setGeometry(56,80,338,64);

}

PawnPromotion::~PawnPromotion()
{
    printf("destructor of PawnPromotionDialog \n");
}

void PawnPromotion::closeEvent(QCloseEvent *bar)
{
    //if no piece is selected, select by default queen
    if(currentIndex == -1) {
        emit promotePawn(indexToType(currentIndex));
    }
    bar->accept();
}

void PawnPromotion::processItem(QListWidgetItem *current) {
    currentIndex = contentsWidget->row(current);
    emit promotePawn(indexToType(currentIndex));
    emit close();
}

Type PawnPromotion::indexToType(int index) {
    Type type = queen;
    switch(index) {
        case 0:
           type = queen;
        break;
        case 1:
           type = rook;
        break;
        case 2:
            type = bishop;
            break;
       case 3:
        type = knight;
        break;
    }
    return type;
}
