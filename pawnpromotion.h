#ifndef PAWNPROMOTION_H
#define PAWNPROMOTION_H

#include <QDialog>
#include <piece.h>

class QListWidget;
class QListWidgetItem;

class PawnPromotion : public QDialog
{
    Q_OBJECT

public:
    PawnPromotion(int color = 0, QWidget *parent = nullptr);
    ~PawnPromotion();

signals:
    void tileSelected();
    void promotePawn(Type pieceType);

public slots:
    void processItem(QListWidgetItem *current);

private:
    Type indexToType(int index);

private:
    QListWidget *contentsWidget;
    void closeEvent(QCloseEvent *bar);
    int pieceColor;
    int currentIndex = -1;
    Type selectedType;
};


#endif // PAWNPROMOTION_H
