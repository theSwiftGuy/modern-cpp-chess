#include "rook.h"

Moves Rook::allMoves(int row, int col) {
    Moves moves;

    auto emptyTrueFunctor = [](int &) { return true; };
    auto decreaseRow = [](int &r) { return r-->0; };
    auto increaseRow = [](int &r) { return r++<7; };
    auto increaseColumn = [](int &c) { return c++<7; };
    auto decreaseColumn = [](int &c) { return c-->0; };

    //movement on row
    findMoves(row, col, decreaseRow, emptyTrueFunctor, _colour, moves);
    findMoves(row, col, increaseRow, emptyTrueFunctor, _colour, moves);

    //movement on column
    findMoves(row, col, emptyTrueFunctor, increaseColumn, _colour, moves);
    findMoves(row, col, emptyTrueFunctor, decreaseColumn, _colour, moves);

    return moves;
}

std::vector<int> Rook::chessPathMoves(int row, int col, int kingRow, int kingCol) {
    std::vector<int> chessPath;
    chessPath.emplace_back(row * 8 + col);

    auto emptyTrueFunctor = [](int &) { return true; };
    auto decreaseRow = [&](int &r) { return r-->kingRow; };
    auto increaseRow = [&](int &r) { return r++<kingRow; };
    auto increaseColumn = [&](int &c) { return c++<kingCol; };
    auto decreaseColumn = [&](int &c) { return c-->kingCol; };

    if(col == kingCol) {
        findChessMoves(row, col, decreaseRow, emptyTrueFunctor, _colour, chessPath);
        findChessMoves(row, col, increaseRow, emptyTrueFunctor, _colour, chessPath);
    }

    if(row == kingRow) {
        findChessMoves(row, col, emptyTrueFunctor, increaseColumn, _colour, chessPath);
        findChessMoves(row, col, emptyTrueFunctor, decreaseColumn, _colour, chessPath);
    }

    return chessPath;
}
