#ifndef ROOK_H
#define ROOK_H
#include "piece.h"

class Rook : public Piece
{
public:
    Rook(Colour colour, Type type = Type::rook) : Piece(colour, type) {}
    virtual ~Rook() override {}
    Moves allMoves(int row, int col) override;
    std::vector<int> chessPathMoves(int row, int col, int kingRow, int kingCol) override;
};

#endif // ROOK_H
