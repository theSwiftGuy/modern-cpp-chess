#include "gamecontroller.h"
#include "piecefactory.h"
#include "pawnpromotion.h"

//helper method
Colour invertColours(Colour actual);

GameController::GameController(QWidget *widget, int spotSize, QObject *parent) : QObject(parent) {
    createChessBoard(widget, spotSize);
    populateChessBoard();
}

void GameController::createChessBoard(QWidget *baseWidget, const int& spotLenght) {
    //Create 64 spots (allocating memories to the objects of Spot class)
    const auto pixelPointYStart = 512;
    const auto pixelPointXStart = 64;
    auto ver = pixelPointYStart;

    for(auto y=0;y<8;y++)
    {
        auto hor = pixelPointXStart;
        for(auto x=0;x<8;x++)
        {
            _spots.emplace_back(std::make_shared<Spot>(x, y, baseWidget));
            _spots.back()->spotDisplay();
            _spots.back()->setGeometry(hor,ver,spotLenght,spotLenght);
            hor += spotLenght;
        }
        ver -= spotLenght;
    }
}

void GameController::populateChessBoard() {
    //white pawns
    for(unsigned int xw=0;xw<8;xw++)
    {
        _spots[xw + 8]->setPiece(PieceFactory::create(pawn, white));
    }

    //black pawns
    for(unsigned int xb=0;xb<8;xb++)
    {
        _spots[xb + 6*8]->setPiece(PieceFactory::create(pawn, black));
    }

    //white remaining pieces
    _spots[0]->setPiece(PieceFactory::create(rook, white));
    _spots[1]->setPiece(PieceFactory::create(knight, white));
    _spots[2]->setPiece(PieceFactory::create(bishop, white));
    _spots[3]->setPiece(PieceFactory::create(queen, white));
    _spots[4]->setPiece(PieceFactory::create(king, white));
    _spots[5]->setPiece(PieceFactory::create(bishop, white));
    _spots[6]->setPiece(PieceFactory::create(knight, white));
    _spots[7]->setPiece(PieceFactory::create(rook, white));

    //black remaining pieces
    _spots[0 + 8*7]->setPiece(PieceFactory::create(rook, black));
    _spots[1 + 8*7]->setPiece(PieceFactory::create(knight, black));
    _spots[2 + 8*7]->setPiece(PieceFactory::create(bishop, black));
    _spots[3 + 8*7]->setPiece(PieceFactory::create(queen, black));
    _spots[4 + 8*7]->setPiece(PieceFactory::create(king, black));
    _spots[5 + 8*7]->setPiece(PieceFactory::create(bishop, black));
    _spots[6 + 8*7]->setPiece(PieceFactory::create(knight, black));
    _spots[7 + 8*7]->setPiece(PieceFactory::create(rook, black));

}

void GameController::promotePawn(Type pieceTypeToPromote) {

    Colour activeColour = activeSpot->getPiece()->get()->_colour;
    _spots[activeSpot->num]->removePiece();
    auto tempPiece = PieceFactory::create(pieceTypeToPromote,activeColour);
    _spots[activeSpot->num]->setPiece(std::move(tempPiece));
}

void GameController::validate(unsigned int indexPath) {

    printf("I released spot index: %d \n", indexPath);
    auto moves = allMovesByColor(_spots[indexPath]->getPiece()->get()->_colour);

    int kingIndex = pieceIndexFor(king,turn);
    if(isAtSpot(moves.movementIndexes(), kingIndex)) {
        printf("chess ... \n");
        _spots[kingIndex]->setStyleSheet("QLabel {background-color: red;}");
    }
}

void GameController::spotReceived(unsigned int indexPath) {
    printf("I received spot index: %d \n", indexPath);

    if(_spots[indexPath]->getPiece() && _spots[indexPath]->getPiece()->get()->_colour == turn) {
        _spots[indexPath]->isSelected(true);
    }

    if(!activeSpot) {
        activeSpot = _spots[indexPath];
    } else {

        if(activeSpot != _spots[indexPath]) { // new spot
            if(activeSpot) {
              //
            }

            previousSpot = activeSpot;
            activeSpot = _spots[indexPath];
            printf("Active spot has [row,col] : [%d,%d] \n", activeSpot->row,activeSpot->col);
            printf("Previous spot has [row,col] : [%d,%d] \n", previousSpot->row,previousSpot->col);

            if(previousSpot->getPiece()) {
                auto indexes = previousSpot->getPiece()->get()->allMoves(previousSpot->row,previousSpot->col);
                for (const int& index : indexes.movementIndexes()) {
                  _spots[index]->spotDisplay();
                }
                _spots[previousSpot->num]->isSelected(false);
            }

        } else { // the same spot selected again
            if(activeSpot->getPiece()) {
                auto indexes = activeSpot->getPiece()->get()->allMoves(activeSpot->row,activeSpot->col);
                for (const int& index : indexes.movementIndexes()) {
                  _spots[index]->spotDisplay();
                }
                _spots[activeSpot->num]->isSelected(false);
            }
           previousSpot.reset();
           activeSpot.reset();
           printf("Active/Previous spot are null \n");

        }
    }

    if(activeSpot) {
        if(activeSpot->getPiece() && activeSpot->getPiece()->get()->_colour == turn) {

            auto indexes = activeSpot->getPiece()->get()->allMoves(activeSpot->row,activeSpot->col);
            auto moveIndexes = indexes.movementIndexes();

            //filtrez indexes in cazul in care regele este in sah, fortand astfel doar anumite mutari disponibile
            auto moves = allMovesByColor(invertColours(turn));

            int kingIndex = pieceIndexFor(king,turn);

            //checking for chess
            if(isAtSpot(moves.movementIndexes(), kingIndex)) {
                if(activeSpot->getPiece()->get()->_type != king) {
                    filterMovementPath(moveIndexes);
                }
             else {

             }
            }

            if(isAtSpot(moves.attackIndexes(), activeSpot->num) && activeSpot->getPiece()->get()->_type != king) {
                filterMovementDefencePathForSpot(activeSpot, moveIndexes);
            }

            if(activeSpot->getPiece()->get()->_type == king)
                 filterKingsMovementPath(moveIndexes);

            for (const int& index : moveIndexes) {
                _spots[index]->colourMovesPath();
            }

        } else {
            movePiece();
        }
    }
}

void GameController::movePiece() {
    if(!previousSpot)
        return;

    if(!previousSpot->getPiece())
        return;

    if (previousSpot->getPiece()->get()->_colour == turn) {

        auto indexes = previousSpot->getPiece()->get()->allMoves(previousSpot->row,previousSpot->col);
        auto moveIndexes = indexes.movementIndexes();

        //indexes trebuie filtrati
        //filtrez indexes in cazul in care regele este in sah, fortand astfel doar anumite mutari disponibile
        auto moves = allMovesByColor(invertColours(turn));

        int kingIndex = pieceIndexFor(king,turn);

        //check if the king is in chess
        if(isAtSpot(moves.movementIndexes(), kingIndex) && previousSpot->getPiece()->get()->_type != king) {
            filterMovementPath(moveIndexes);
        }

        if(isAtSpot(moves.attackIndexes(), previousSpot->num) && previousSpot->getPiece()->get()->_type != king) {
            filterMovementDefencePathForSpot(previousSpot, moveIndexes);
        }

        if(previousSpot->getPiece()->get()->_type == king)
            filterKingsMovementPath(moveIndexes);

        auto result = std::find(std::begin(moveIndexes), std::end(moveIndexes), activeSpot->num);

        if (result != std::end(moveIndexes)) {

            for (const int& index : moveIndexes) {
              _spots[index]->spotDisplay();
            }

            _spots[activeSpot->num]->removePiece();

            auto piesa = previousSpot->getPiece();
            auto culoare = piesa->get()->_colour;
            auto tip = piesa->get()->_type;
            auto tempPiece = PieceFactory::create(tip,culoare);

            //TODO - must be refactored
            if ( piesa->get()->_type == pawn) {
                if(activeSpot->num >= 56 || activeSpot->num <= 7) {
                    printf("promotion \n");
                    PawnPromotion *dialog = new PawnPromotion(culoare);
                    dialog->open();
                }
                if(abs(activeSpot->num - previousSpot->num) == 9) {
                    if(culoare == white && _spots[previousSpot->num + 1]->getPiece() &&
                            _spots[previousSpot->num + 1]->getPiece()->get()->enPassant == true) {
                    _spots[previousSpot->num + 1]->removePiece();//aici alb muta pe negru la dr
                    }
                    if(culoare == black && _spots[previousSpot->num - 1]->getPiece() &&
                            _spots[previousSpot->num - 1]->getPiece()->get()->enPassant == true) {
                    _spots[previousSpot->num - 1]->removePiece();//negru la st peste alb
                    }
                } else if(abs(activeSpot->num - previousSpot->num) == 7){
                    if(culoare == white && _spots[previousSpot->num - 1]->getPiece() &&
                            _spots[previousSpot->num - 1]->getPiece()->get()->enPassant == true) {
                    _spots[previousSpot->num - 1]->removePiece();//aici alb muta negru la st
                    }
                    if(culoare == black && _spots[previousSpot->num + 1]->getPiece() &&
                            _spots[previousSpot->num + 1]->getPiece()->get()->enPassant == true) {
                      _spots[previousSpot->num + 1]->removePiece();//negru la dr muta peste alb
                    }
                } else if(abs(activeSpot->num - previousSpot->num) == 16 &&
                        ((_spots[activeSpot->num - 1]->getPiece()
                          && _spots[activeSpot->num - 1]->getPiece()->get()->_type == pawn
                          && _spots[activeSpot->num - 1]->getPiece()->get()->_colour != culoare)
                         || (_spots[activeSpot->num + 1]->getPiece()
                             && _spots[activeSpot->num + 1]->getPiece()->get()->_type == pawn
                             && _spots[activeSpot->num + 1]->getPiece()->get()->_colour != culoare))){
                    tempPiece->enPassant = true;
                }
            }

            _spots[activeSpot->num]->setPiece(std::move(tempPiece));
            _spots[previousSpot->num]->removePiece();
            _spots[previousSpot->num]->isSelected(false);

            //update turn after movement is done
            updateTurn();
        }
    }
}

void GameController::updateTurn() {
    turn = invertColours(turn);
}

bool GameController::isAtSpot(const std::vector<int>& indexes, int currentIndex) {

    bool isAttack = false;
    auto result = std::find(indexes.begin(), indexes.end(), currentIndex);

    if (result != std::end(indexes)) {
        isAttack = true;
    }
    return isAttack;
}

int GameController::pieceIndexFor(Type type, Colour colour) {

    int index = -1;
    if ( const auto itr = std::find_if(_spots.begin(), _spots.end(), [&] (const std::shared_ptr<Spot>& s) {
                                       if(!s->getPiece())
                                            return false;
                                       return s->getPiece()->get()->isTypeAndColour(type, colour);
        });
           itr != _spots.end())
       {
           index = itr->get()->num;
       }

    return index;
}

std::vector<int> GameController::chessGeneratorIndexes(Colour colour, int kingIndex) {

    std::vector<int> locations;
    //for each piece of the same colour, I will check if is generating chess
    std::for_each(_spots.begin(), _spots.end(), [&] (const std::shared_ptr<Spot>& s)
    {
        if(s->getPiece() && s->getPiece()->get()->_colour == colour) {
            auto indexes = s->getPiece()->get()->allMoves(s->row,s->col);

            if(isAtSpot(indexes.attackedIndexes, kingIndex)) { //maybe I have to use .attackIndexes() ??? to check this!
                locations.emplace_back(s->num);
            }
        }
    });

    return locations;
}

Moves GameController::allMovesByColor(Colour colour) {

    Moves moves;
    std::vector<int> attack;
    std::vector<int> defence;
    std::vector<int> pawnMoveMode;
    std::vector<int> pawnAttackMode;

    std::for_each(_spots.begin(), _spots.end(), [&] (const std::shared_ptr<Spot>& s)
    {
        if(s->getPiece() && s->getPiece()->get()->_colour == colour) {
            auto indexes = s->getPiece()->get()->allMoves(s->row,s->col);
            attack.insert(attack.end(),indexes.attackedIndexes.begin(),indexes.attackedIndexes.end());
            defence.insert(defence.end(),indexes.protectedIndexes.begin(),indexes.protectedIndexes.end());
            pawnMoveMode.insert(pawnMoveMode.end(),indexes.pawnMoveModeIndexes.begin(),indexes.pawnMoveModeIndexes.end());
            pawnAttackMode.insert(pawnAttackMode.end(),indexes.pawnAttackModeIndexes.begin(),indexes.pawnAttackModeIndexes.end());
        }
    });

    std::sort(attack.begin(), attack.end());
    std::sort(defence.begin(), defence.end());
    std::sort(pawnMoveMode.begin(), pawnMoveMode.end());
    std::sort(pawnAttackMode.begin(), pawnAttackMode.end());

    auto last = std::unique(attack.begin(), attack.end());
    attack.erase(last, attack.end());

    last = std::unique(defence.begin(), defence.end());
    defence.erase(last,defence.end());

    last = std::unique(pawnMoveMode.begin(), pawnMoveMode.end());
    pawnMoveMode.erase(last,pawnMoveMode.end());

    last = std::unique(pawnAttackMode.begin(), pawnAttackMode.end());
    pawnAttackMode.erase(last,pawnAttackMode.end());

    moves.attackedIndexes = attack;
    moves.protectedIndexes = defence;
    moves.pawnMoveModeIndexes = pawnMoveMode;
    moves.pawnAttackModeIndexes = pawnAttackMode;

    return moves;
}

//called only if king is in chess
void GameController::filterMovementPath(std::vector<int> &moves) {

    std::vector<int> v_intersection;

    int kingIndex = pieceIndexFor(king,turn);
    auto indexes = chessGeneratorIndexes(invertColours(turn), kingIndex);
    for (const int& index : indexes) {
        //_spots[index]->setStyleSheet("QLabel {background-color: yellow;}");
        auto chessPath = _spots[index]->getPiece()->get()->chessPathMoves(_spots[index]->row,_spots[index]->col, _spots[kingIndex]->row, _spots[kingIndex]->col);
        std::sort(moves.begin(), moves.end());
        std::sort(chessPath.begin(), chessPath.end());
        std::set_intersection(moves.begin(), moves.end(),
                               chessPath.begin(), chessPath.end(),
                               std::back_inserter(v_intersection));
    }

    moves.clear();
    moves = v_intersection;
}

void GameController::filterMovementDefencePathForSpot(const std::shared_ptr<Spot>& spot, std::vector<int> &moveIndexes) {

    auto removedPiece = spot->getPiece();
    auto colour = removedPiece->get()->_colour;
    auto type = removedPiece->get()->_type;

    _spots[spot->num]->removePiece();

    if(!_spots[spot->num]->getPiece()) {

        int kingIndex = pieceIndexFor(king,turn);
        auto indexes = chessGeneratorIndexes(invertColours(turn), kingIndex);
        if(indexes.capacity() != 0) {
            auto chessPath = _spots[indexes[0]]->getPiece()->get()->chessPathMoves(_spots[indexes[0]]->row,_spots[indexes[0]]->col,
                                                                                    _spots[kingIndex]->row, _spots[kingIndex]->col);
            if(isAtSpot(chessPath, spot->num)) {

                std::vector<int> v_intersection;
                std::sort(moveIndexes.begin(), moveIndexes.end());
                std::sort(chessPath.begin(), chessPath.end());
                std::set_intersection(moveIndexes.begin(), moveIndexes.end(),
                                       chessPath.begin(), chessPath.end(),
                                       std::back_inserter(v_intersection));
                moveIndexes.clear();
                moveIndexes = v_intersection;
            }
        }
    }

    _spots[spot->num]->setPiece(PieceFactory::create(type, colour));
}

void GameController::filterKingsMovementPath(std::vector<int> &moves) {

    std::vector<int> v_intersection;

    int kingIndex = pieceIndexFor(king,turn);

    auto indexes = allMovesByColor(invertColours(turn));

    auto allProtectedIndexes = indexes.protectedIndexes;
    std::sort(moves.begin(), moves.end());
    std::sort(allProtectedIndexes.begin(), allProtectedIndexes.end());
    std::set_intersection(moves.begin(), moves.end(),
                           allProtectedIndexes.begin(), allProtectedIndexes.end(),
                           std::back_inserter(v_intersection));

    std::vector<int> diff;
    std::set_difference(moves.begin(), moves.end(), v_intersection.begin(), v_intersection.end(),
                        std::inserter(diff, diff.begin()));

    moves.clear();
    moves = diff;

    //filter for attack positions
    auto allAttackedIndexes = indexes.attackIndexes();//cml
    std::sort(moves.begin(), moves.end());
    std::sort(allAttackedIndexes.begin(), allAttackedIndexes.end());
    v_intersection.clear();
    std::set_intersection(moves.begin(), moves.end(),
                           allAttackedIndexes.begin(), allAttackedIndexes.end(),
                           std::back_inserter(v_intersection));

    diff.clear();
    std::set_difference(moves.begin(), moves.end(), v_intersection.begin(), v_intersection.end(),
                        std::inserter(diff, diff.begin()));

    moves.clear();
    moves = diff;

    //get index that contain the piece that is generating check
    auto positions = chessGeneratorIndexes(invertColours(turn), kingIndex);

    auto temporaryKingPiece = _spots[kingIndex]->getPiece();
    auto colour = temporaryKingPiece->get()->_colour;
    auto type = temporaryKingPiece->get()->_type;
    auto tempPiece = PieceFactory::create(type,colour);

    _spots[kingIndex]->removePiece();
       if(!_spots[kingIndex]->getPiece()) {

            if(positions.capacity() != 0) {
                auto chessPath = _spots[positions[0]]->getPiece()->get()->allMoves(_spots[positions[0]]->row,_spots[positions[0]]->col);

                std::vector<int> removeIndexes;
                std::sort(chessPath.attackedIndexes.begin(), chessPath.attackedIndexes.end());
                std::sort(moves.begin(), moves.end());
                std::set_intersection(moves.begin(), moves.end(),
                                       chessPath.attackedIndexes.begin(), chessPath.attackedIndexes.end(),
                                       std::back_inserter(removeIndexes));

                if(removeIndexes.capacity() != 0) {
                    diff.clear();
                    std::set_difference(moves.begin(), moves.end(), removeIndexes.begin(), removeIndexes.end(),
                                        std::inserter(diff, diff.begin()));

                    moves.clear();
                    moves = diff;
                }

          }
    }

    _spots[kingIndex]->setPiece(std::move(tempPiece));
}

//helper method
Colour invertColours(Colour actual) {
    Colour now = white;
    if(actual == now)
        now = black;
    return now;
}
