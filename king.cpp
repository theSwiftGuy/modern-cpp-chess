#include "king.h"

Moves King::allMoves(int row, int col) {
    int r,c;
    Moves moves;

    r=row;
    c=col;
    if(r-1>=0) {
        addMoves(static_cast<unsigned int>((r-1) * 8 + c), _colour, moves);
    }

    if(r+1<=7) {
        addMoves(static_cast<unsigned int>((r+1) * 8 + c), _colour, moves);
    }

    if(c-1>=0) {
        addMoves(static_cast<unsigned int>(r * 8 + (c-1)), _colour, moves);
    }

    if(c+1<=7) {
        addMoves(static_cast<unsigned int>(r * 8 + (c+1)), _colour, moves);
    }

    if(r-1>=0 && c-1>=0) {
        addMoves(static_cast<unsigned int>((r-1) * 8 + (c-1)), _colour, moves);
    }

    if(r-1>=0 && c+1<=7) {
        addMoves(static_cast<unsigned int>((r-1) * 8 + (c+1)), _colour, moves);
    }

    if(r+1<=7 && c-1>=0) {
        addMoves(static_cast<unsigned int>((r+1) * 8 + (c-1)), _colour, moves);
    }

    if(r+1<=7 && c+1<=7) {
        addMoves(static_cast<unsigned int>((r+1) * 8 + (c+1)), _colour, moves);
    }

    return moves;
}

std::vector<int> King::chessPathMoves([[maybe_unused]]int row,[[maybe_unused]] int col,[[maybe_unused]] int kingRow,[[maybe_unused]] int kingCol) {
    return {};
}
