#include "knight.h"

Moves Knight::allMoves(int row, int col) {
    Moves moves;

    int r,c;
    r=row;
    c=col;

    if(r-2>=0 && c-1>=0) {
        addMoves(static_cast<unsigned int>((r-2) * 8 + (c-1)), _colour, moves);
    }

    if(r-2>=0 && c+1<=7) {
        addMoves(static_cast<unsigned int>((r-2) * 8 + (c+1)), _colour, moves);
    }

    if(r-1>=0 && c-2>=0) {
        addMoves(static_cast<unsigned int>((r-1) * 8 + (c-2)), _colour, moves);
    }

    if(r-1>=0 && c+2<=7) {
        addMoves(static_cast<unsigned int>((r-1) * 8 + (c+2)), _colour, moves);
    }

    if(r+2<=7 && c+1<=7) {
        addMoves(static_cast<unsigned int>((r+2) * 8 + (c+1)), _colour, moves);
    }

    if(r+2<=7 && c-1>=0) {
        addMoves(static_cast<unsigned int>((r+2) * 8 + (c-1)), _colour, moves);
    }

    if(r+1<=7 && c-2>=0) {
        addMoves(static_cast<unsigned int>((r+1) * 8 + (c-2)), _colour, moves);
    }

    if(r+1<=7 && c+2<=7) {
        addMoves(static_cast<unsigned int>((r+1) * 8 + (c+2)), _colour, moves);
    }

    return moves;
}

std::vector<int> Knight::chessPathMoves(int row, int col,[[maybe_unused]] int kingRow,[[maybe_unused]] int kingCol) {
    return {row * 8 + col};
}
