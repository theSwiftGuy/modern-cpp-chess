#ifndef KING_H
#define KING_H
#include "piece.h"

class King : public Piece
{
public:
    King(Colour colour, Type type = Type::king) : Piece(colour, type) {}
    virtual ~King() override {}
    Moves allMoves(int row, int col) override;
    std::vector<int> chessPathMoves(int row, int col, int kingRow, int kingCol) override;
};

#endif // KING_H
