#include "spot.h"
#include "gamecontroller.h"

Spot::Spot(int column, int row, QWidget *pParent, Qt::WindowFlags f)
    : QLabel(pParent, f), row(row), col(column) {
    color = (row+column)%2;
    num = col + 8 * row;
}

Spot::Spot(const QString &text, int column, int row, QWidget *pParent, Qt::WindowFlags f)
    : QLabel(text, pParent, f), row(row), col(column) {
    color = (row+column)%2;
    num = col + 8 * row;

}

Spot::Spot(const Spot &obj) {
    this->col = obj.col;
    this->row = obj.row;
    this->color = obj.color;
    this->num = obj.num;
}

Spot& Spot::operator=(const Spot &other) {
    // check for self-assignment
    if(this != &other)
    {
        color = other.color;
        num = other.num;
        col = other.col;
        row = other.row;
    }
    return *this;
}

void Spot::mousePressEvent([[maybe_unused]] QMouseEvent *event) {
    QObject::connect(this, &Spot::spotSelected, &GameController::get(), &GameController::spotReceived);

    if(_piece.has_value()) {
        printf(" I pressed on a piece at [%d,%d] = %d \n", row, col, num);
    } else {
        printf(" I pressed at [%d,%d] = %d without piece \n", row, col, num);
    }

    emit spotSelected(static_cast<unsigned int>(num));

    QObject::disconnect(this, &Spot::spotSelected, &GameController::get(), &GameController::spotReceived);
}

void Spot::mouseReleaseEvent([[maybe_unused]] QMouseEvent *event) {
    QObject::connect(this, &Spot::spotReleased, &GameController::get(), &GameController::validate);
    if(_piece.has_value()) {
         emit spotReleased(static_cast<unsigned int>(num));
    }
    QObject::disconnect(this, &Spot::spotReleased, &GameController::get(), &GameController::validate);
}

void Spot::setPiece(std::shared_ptr<Piece> piece) {
    //changing value with emplace
    _piece.emplace(std::move(piece));
    //this->setPixmap(QPixmap(_piece->get()->imagePath().data()));
    this->setPixmap(QPixmap(_piece->get()->imagePath().c_str()));
}

std::optional<std::shared_ptr<Piece>> Spot::getPiece() {
    if (!_piece.has_value())
        return { };

    return _piece;
}

void Spot::removePiece() {
    if(_piece.has_value()) {
        _piece.reset();
        this->clear();
    }
}

void Spot::spotDisplay() {
    this->setStyleSheet("QLabel {background-color: rgb(173, 136, 94);}:hover{background-color: rgb(160, 152, 52);}");//dark color
    if(this->color)
        this->setStyleSheet("QLabel {background-color: rgb(238, 216, 176);}:hover{background-color: rgb(160, 152, 52);}");//light color
}

void Spot::isSelected(bool value) {
    if(value)
        this->setStyleSheet("QLabel {background-color: green;}");
    else
        this->spotDisplay();
}

void Spot::colourMovesPath() {
  this->setStyleSheet("QLabel {background-color: orange;}");
}
