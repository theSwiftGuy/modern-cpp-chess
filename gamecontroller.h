#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <QObject>
#include <spot.h>

class GameController : public QObject
{
    Q_OBJECT
public:
    static GameController &get(QWidget* widget = nullptr, int spotSize = 64) {
        static GameController gameController(widget, spotSize);
        return gameController;
    }
    void populateChessBoard();

signals:

public slots:
    void validate(unsigned int indexPath);
    void spotReceived(unsigned int indexPath);
    void promotePawn(Type pieceType);

private:
    GameController(QWidget* widget = nullptr, int spotSize = 64, QObject *parent = nullptr);
    GameController(GameController const &) = delete;
    GameController &operator=(GameController const &) = delete;

    void createChessBoard(QWidget *baseWidget, const int& spotLenght);
    void movePiece();
    void updateTurn();
    bool isAtSpot(const std::vector<int>& indexes, int currentIndex);
    int pieceIndexFor(Type type, Colour colour);
    std::vector<int> chessGeneratorIndexes(Colour colour, int kingIndex);
    Moves allMovesByColor(Colour colour);
    void filterMovementPath(std::vector<int> &moves);
    void filterMovementDefencePathForSpot(const std::shared_ptr<Spot>& spot, std::vector<int> &moves);
    void filterKingsMovementPath(std::vector<int> &moves);

private:
    std::shared_ptr<Spot> previousSpot = nullptr;
    std::shared_ptr<Spot> activeSpot = nullptr;
    Colour turn = white;

public:
    std::vector<std::shared_ptr<Spot>> _spots;
};

#endif // GAMECONTROLLER_H
