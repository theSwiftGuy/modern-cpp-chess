#ifndef PAWN_H
#define PAWN_H
#include "piece.h"

class Pawn : public Piece
{
public:
    Pawn(Colour colour, Type type = Type::pawn) : Piece(colour, type) {}
    virtual ~Pawn() override {}
    Moves allMoves(int row, int col) override;
    std::vector<int> chessPathMoves(int row, int col, int kingRow, int kingCol) override;
};

#endif // PAWN_H
